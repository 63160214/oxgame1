
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Admin
 */
public class OX {
    static char table[][]={{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char currentPlayer='O';
    static int row,col; 
     static boolean finish = false;
    static int count = 0;
    static Scanner sc = new Scanner(System.in);
    
    public static void main(String[] args){
         while (true) {
            showTable();
            showTurn();
            inputRowCol();
            process();
            if (finish) {
                break;
            }
        }
    }
    
    public static void showTable(){
        for(int r=0;r<table.length;r++){
            for(int c=0;c<table[r].length;c++){
                System.out.print(table[r][c]);
            }
            System.out.println("");
        }
        
    }

    public static void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public static void showTurn() {
        System.out.println("Turn " + currentPlayer);
    }

    public static void inputRowCol() {
        System.out.println("Please input row,col: ");
        row = sc.nextInt();
        col = sc.nextInt();
    }
    
    public static void process() {
        if (setTable()) {
            if (checkWin(table,currentPlayer,row,col)) {
                finish = true;
                showTable();
                System.out.println(">>>" + currentPlayer + " Win<<<");
                return;
            }
            if (checkDraw(count)) {
                finish = true;
                showTable();
                System.out.println(">>>Draw<<<");
                return;
            }
            count++;
            switchPlayer();
        }

    }

     public static boolean setTable() {
        table[row - 1][col - 1] = currentPlayer;
        return true;
    }

    public static boolean checkWin(char[][] table,char currrentPlayer,int row,int col) {
        
        if (checkVertical(table,currentPlayer,col)) {
            return true;
        } else if (checkHorizontal(table,currentPlayer,row)) {
            return true;
        } else if (checkX(table,currentPlayer)) {
            return true;
        }

        return false;
    }

    private static boolean checkDraw(int count) {
        if (count == 8 && !checkVertical(table,currentPlayer,col)&& !checkHorizontal(table,currentPlayer,row)&& !checkX(table,currentPlayer)) {
            return true;
         }
        return false;
    }

    public static void switchPlayer() {
        if (currentPlayer == 'O') {
            currentPlayer = 'X'; //switchPlayer()
        } else {
            currentPlayer = 'O';
        }
    }

     public static boolean checkVertical(char[][] table,char currrentPlayer,int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkHorizontal(char[][] table,char currrentPlayer,int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

     public static boolean checkX(char[][] table,char currrentPlayer) {
        if (checkX1(table,currrentPlayer)) {
            return true;
        } else if (checkX2(table,currrentPlayer)) {
            return true;
        }
        return false;
    }

    public static boolean checkX1(char[][] table,char currrentPlayer) { //11,22,33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }

    public static boolean checkX2(char[][] table,char currrentPlayer) { //13,22,31 -> 02,11,20
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlayer) {
                return false;
            }
        }
        return true;
    }
    
    
   
}
