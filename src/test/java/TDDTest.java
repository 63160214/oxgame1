/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Admin
 */
public class TDDTest {
    
    public TDDTest() {
        
    }
    @Test
    public void testCheckVarticlePlayerOCol1Win(){
        char table[][]={{'O','-','-'},
                        {'O','-','-'},
                        {'O','-','-'}};
        char currentPlayer='O';
        int col=1; 
        assertEquals(true,OX.checkVertical(table,currentPlayer,col));
    } 
    @Test
    public void testCheckVarticlePlayerOCol2Win(){
        char table[][]={{'-','O','-'},
                        {'-','O','-'},
                        {'-','O','-'}};
        char currentPlayer='O';
        int col=1; 
        assertEquals(true,OX.checkVertical(table,currentPlayer,col));
    }   
    @Test
     public void testCheckPlayerOCol2Win(){
        char table[][]={{'-','O','-'},
                        {'-','O','-'},
                        {'-','O','-'}};
        char currentPlayer='O';
        int row=1,col=2; 
        assertEquals(true,OX.checkWin(table,currentPlayer,row,col));
    }   
    
     @Test
    public void testCheckVarticlePlayerOCol3Win(){
        char table[][]={{'-','-','O'},
                        {'-','-','O'},
                        {'-','-','O'}};
        char currentPlayer='O';
        int col=3; 
        assertEquals(true,OX.checkVertical(table,currentPlayer,col));
    }   
    
    @Test
    public void testCheckcheckHorizontalPlayerORow1Win(){
        char table[][]={{'O','O','O'},
                        {'-','-','-'},
                        {'-','-','-'}};
        char currentPlayer='O';
        int row=1; 
        assertEquals(true,OX.checkHorizontal(table,currentPlayer,row));
    }   
    
    @Test
    public void testCheckHorizontalPlayerORow2Win(){
        char table[][]={{'-','-','-'},
                        {'O','O','O'},
                        {'-','-','-'}};
        char currentPlayer='O';
        int row=2; 
        assertEquals(true,OX.checkHorizontal(table,currentPlayer,row));
    }   
    
    @Test
    public void testCheckHorizontalPlayerORow3Win(){
        char table[][]={{'-','-','-'},
                        {'-','-','-'},
                        {'O','O','O'}};
        char currentPlayer='O';
        int row=3; 
        assertEquals(true,OX.checkHorizontal(table,currentPlayer,row));
    }   
    
    @Test
    public void testCheckX1PlayerOWin(){
        char table[][]={{'O','-','-'},
                        {'-','O','-'},
                        {'-','-','O'}};
        char currentPlayer='O';
         
        assertEquals(true,OX.checkX1(table,currentPlayer));
    }   
    
    @Test
    public void testCheckX2PlayerOWin(){
        char table[][]={{'-','-','O'},
                        {'-','O','-'},
                        {'O','-','-'}};
        char currentPlayer='O';
         
        assertEquals(true,OX.checkX2(table,currentPlayer));
    }   
}
